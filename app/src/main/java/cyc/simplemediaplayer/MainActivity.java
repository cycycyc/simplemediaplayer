package cyc.simplemediaplayer;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    VideoView videoView;
    LinearLayout layout;
    EditText editText;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        videoView = (VideoView)findViewById(R.id.videoView);
        layout = (LinearLayout)findViewById(R.id.controlLayout);
        editText = (EditText)findViewById(R.id.editText);
        button = (Button)findViewById(R.id.button);

        SharedPreferences preferences = getSharedPreferences("simplePlayer", MODE_PRIVATE);
        editText.setText(preferences.getString("uri", "/sdcard/vid_bigbuckbunny.mp4"));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = getSharedPreferences("simplePlayer", MODE_PRIVATE).edit();
                editor.putString("uri", editText.getText().toString());
                editor.commit();

                videoView.setVideoPath(editText.getText().toString());
                videoView.start();
                layout.setVisibility(View.INVISIBLE);
            }
        });
    }
}
